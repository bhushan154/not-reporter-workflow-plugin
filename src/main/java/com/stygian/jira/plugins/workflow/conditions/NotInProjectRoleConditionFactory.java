/*
Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
*/
package com.stygian.jira.plugins.workflow.conditions;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginConditionFactory;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.ConditionDescriptor;

import java.util.Map;

/*
This is the factory class responsible for dealing with the UI for the post-function.
This is typically where you put default values into the velocity context and where you store user input.
 */

public class NotInProjectRoleConditionFactory extends AbstractWorkflowPluginFactory implements WorkflowPluginConditionFactory
{
    public static final String PROJECT_ROLES="PROJECT_ROLES";
    public static final String SELECTED_PROJECT_ROLES="SELECTED_PROJECT_ROLES";
    private final ProjectRoleManager projectRoleManager;

    public NotInProjectRoleConditionFactory(ProjectRoleManager projectRoleManager) {
        this.projectRoleManager = projectRoleManager;
    }

    protected void getVelocityParamsForInput(Map velocityParams)
    {
        //the default message
        velocityParams.put(PROJECT_ROLES,projectRoleManager.getProjectRoles());
    }

    protected void getVelocityParamsForEdit(Map velocityParams, AbstractDescriptor descriptor)
    {
        getVelocityParamsForInput(velocityParams);
        getVelocityParamsForView(velocityParams, descriptor);
    }

    protected void getVelocityParamsForView(Map velocityParams, AbstractDescriptor descriptor)
    {
        if (!(descriptor instanceof ConditionDescriptor))
        {
            throw new IllegalArgumentException("Descriptor must be a ConditionDescriptor.");
        }

        ConditionDescriptor conditionDescriptor = (ConditionDescriptor) descriptor;

        velocityParams.put(SELECTED_PROJECT_ROLES, conditionDescriptor.getArgs().get(SELECTED_PROJECT_ROLES));
    }

    public Map getDescriptorParams(Map conditionParams)
    {
        // Process The map
        String[] value = (String[])conditionParams.get(SELECTED_PROJECT_ROLES);
        StringBuilder stringBuilder = new StringBuilder();
        for(String string:value){
            stringBuilder.append(string + ",");
        }
        return EasyMap.build(SELECTED_PROJECT_ROLES, stringBuilder.toString().substring(0,stringBuilder.lastIndexOf(",")));
    }
}
