package com.stygian.jira.plugins.workflow.conditions;

import com.atlassian.jira.security.JiraAuthenticationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.workflow.condition.AbstractJiraCondition;
import com.opensymphony.module.propertyset.PropertySet;

import java.util.Map;

public class NotAssigneeCondition extends AbstractJiraCondition
{
    private static final Logger log = LoggerFactory.getLogger(NotAssigneeCondition.class);
    private JiraAuthenticationContext jiraAuthenticationContext;

    public NotAssigneeCondition(JiraAuthenticationContext jiraAuthenticationContext){
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    public boolean passesCondition(Map transientVars, Map args, PropertySet ps)
    {
        Issue issue = getIssue(transientVars);

        return issue.getAssignee() != jiraAuthenticationContext.getLoggedInUser();
    }
}
