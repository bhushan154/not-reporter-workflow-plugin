/*
Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
*/
package com.stygian.jira.plugins.workflow.conditions;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.jira.security.roles.ProjectRole;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.workflow.condition.AbstractJiraCondition;
import com.opensymphony.module.propertyset.PropertySet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class NotInGroupCondition extends AbstractJiraCondition
{
    private static final Logger log = LoggerFactory.getLogger(NotInProjectRoleCondition.class);
    public static final String GROUPS="GROUPS";
    public static final String SELECTED_GROUPS="SELECTED_GROUPS";
    private final GroupManager groupManager;
    private final JiraAuthenticationContext authContext;
    private final CustomFieldManager customFieldManager;

    public NotInGroupCondition(GroupManager groupManager,
                                     JiraAuthenticationContext authContext,
                                     CustomFieldManager customFieldManager) {
        this.groupManager = groupManager;
        this.authContext = authContext;
        this.customFieldManager = customFieldManager;
    }

    public boolean passesCondition(Map transientVars, Map args, PropertySet ps)
    {
        Issue issue = (Issue) transientVars.get("issue");
        User user = authContext.getLoggedInUser();
        Project project = issue.getProjectObject();
        String selectedProjectRoles = (String) args.get(SELECTED_GROUPS);
        List<String> selectedProjectRolesList = Arrays.asList(selectedProjectRoles.split("\\s*,\\s*"));
        List<String> errorMessages = new ArrayList<String>();
        boolean allowed = true;
        for(String group:selectedProjectRolesList)
        {
            groupManager.getGroup(group);
            if(groupManager.isUserInGroup(user,groupManager.getGroup(group)))
            {
                allowed = false;
            }
        }

        return allowed;
    }
}
