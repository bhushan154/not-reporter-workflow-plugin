package com.stygian.jira.plugins.workflow.validators;

import com.atlassian.jira.security.JiraAuthenticationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.issue.Issue;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.workflow.Validator;
import com.opensymphony.workflow.InvalidInputException;
import java.util.Map;

public class NotReporterValidator implements Validator
{
    private static final Logger log = LoggerFactory.getLogger(NotReporterValidator.class);
    private JiraAuthenticationContext jiraAuthenticationContext;

    public NotReporterValidator(JiraAuthenticationContext jiraAuthenticationContext){
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    public void validate(Map transientVars, Map args, PropertySet ps) throws InvalidInputException
    {

        Issue issue = (Issue) transientVars.get("issue");

        if(issue.getReporter() == jiraAuthenticationContext.getLoggedInUser()) {
            throw new InvalidInputException("The reporter of this issue cannot transition this issue.");
        }
    }
}
